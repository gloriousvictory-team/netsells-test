<?php

namespace App;

class IntegerConversion implements IntegerConversionInterface
{
    private $_lookup = [
    'I' => 1,
    'IV' => 4,
    'V' => 5,
    'IX' => 9,
    'X' => 10,
    'XL' => 40,
    'L' => 50,
    'XC' => 90,
    'C' => 100,
    'CD' => 400,
    'D' => 500,
    'CM' => 900,
    'M' => 1000,
  ];

    public function toRomanNumerals($inputInteger)
    {
        if ($inputInteger < 1 || $inputInteger > 3999) {
            return false;
        }

        // Start by sorting the lookup array from big to small
        $lookupValues = array_reverse($this->_lookup);

        // Place holder for final Roman Numeral;
        $romanNumeral = "";

        // Initial value for while loop
        $number = $inputInteger;

        while ($number > 0) {
            foreach ($lookupValues as $key => $value) {

                // Loop through until we find the biggest lookup value that we can
                // fit inside our initial integer
                if ($number >= $value) {
                    $romanNumeral .= $key; // append the roman numeral
                    $number -= $value; // subtract it's value from the starting number
                    break; // start the loop again
                }
            }
        }

        return $romanNumeral;
    }
}
