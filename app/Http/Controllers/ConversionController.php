<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use League\Fractal\Manager;

use App\IntegerConversion;
use App\Conversion;
use App\Transformers\ConversionTransformer;
use App\Transformers\ConversionListTransformer;
use App\Transformers\ConversionTopTenTransformer;

class ConversionController extends Controller
{
    protected $integerConversion;
    protected $fractalManager;

    public function __construct(IntegerConversion $converter, Manager $fractal)
    {

      // Create a top level instance somewhere
        $this->fractalManager = $fractal;
        $this->integerConversion = $converter;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // List recently converted integers
        // Unsure of what the brief meant exact but went with only returning
        // the integer values!

        $conversions = Conversion::groupBy('converted_integer')->select('*')->get();

        return fractal($conversions, new ConversionListTransformer())->respond();
    }

    public function topTen()
    {

        // List top ten integers by most requested

        $conversions = Conversion::groupBy('converted_integer')->select('*', \DB::raw('count(*) as total'))->get()->sortByDesc('total')->take(10);

        return fractal($conversions, new ConversionTopTenTransformer())->respond();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validate($request, [
          'to_convert' => 'required|min:1|max:3999',
      ]);

        $conversionInteger = $request->input('to_convert');
        if ($romanNumeral = $this->integerConversion->toRomanNumerals($conversionInteger)) {
            $conversion = Conversion::create(['converted_integer' => $conversionInteger, 'roman_numeral' => $romanNumeral]);

            return fractal($conversion, new ConversionTransformer())->respond();
        }
    }
}
