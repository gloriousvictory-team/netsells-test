<?php

namespace App\Transformers;

use App\Conversion;
use League\Fractal;
use Carbon\Carbon;

class ConversionListTransformer extends Fractal\TransformerAbstract
{
    public function transform(Conversion $conversion)
    {
        return [
            'id'							 => $conversion->id,
            'convertedInteger' => (int) $conversion->converted_integer
        ];
    }
}
