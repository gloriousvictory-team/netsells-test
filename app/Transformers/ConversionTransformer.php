<?php

namespace App\Transformers;

use App\Conversion;
use League\Fractal;
use Carbon\Carbon;

class ConversionTransformer extends Fractal\TransformerAbstract
{
    public function transform(Conversion $conversion)
    {
        return [
            'id'               => (int) $conversion->id,
          	'convertedInteger' => (int) $conversion->converted_integer,
            'romanNumeral'     => $conversion->roman_numeral,
          	'createdAt'        => Carbon::createFromFormat('Y-m-d H:i:s', $conversion->created_at)->format('d-m-Y')
        ];
    }
}
